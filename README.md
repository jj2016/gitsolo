### 什么是“gitsolo”？
---------------------------------------
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;“gitsolo”是知启蒙团队开源的极简Git服务器，纯Java开发，本地文件数据库，只依赖JDK，一键启动（zhiqim.exe/zhiqim.lix），支持HTT(S)协议、多项目多成员权限管理和二次开发。。

<br>

### gitsolo有什么优点？
---------------------------------------
1、想搭一个Git服务器，又觉得GitLab很麻烦的公司或个人，选择gitsolo绝对错不了。<br>
2、gitsolo从1.6.0版本开始，采用木兰宽松许可证第2版发布开源版本。有不清楚的进群或在<br>
3、gitsolo只依赖JDK和ZhiqimDK，安装简单到爆，特别是有经验的Java程序员，配置好boot.home，一键启动（zhiqim.exe/zhiqim.lix）。<br>
4、独立工程，不需要WEB容器，什么Tomcat/Jetty不需要的。<br>
5、有HTTP(S)协议就够了，什么ssh协议配置太麻烦。gitsolo的多项目/多成员管理很棒的。<br>
6、还有一个特点就是会检查提交者和提交者邮箱，这个要注意啦，提交者必须是gitsolo的用户名，否则提交不了，为什么这么设计呢？因为这么控制了之后，可以避免别人用自己的账号提交啦，哈哈哈。<br>
<br>

### gitsolo安装指南
---------------------------------------
1、要求JDK1.7+,其详细安装教程请前往[【JDK安装教程】](https://zhiqim.org/document/bestcase/jdk.htm)。<br>
2、进往下载发行版：下载请点击[【gitsolo发行版】](https://zhiqim.org/project/zhiqim_products/gitsolo/release.htm)。<br>
<br>

### gitsolo配置启动图解
---------------------------------------
一、目录结构，在版本中下载gitsolo.zip后解压出来的目录结构：<br><br>
![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_01.png "目录结构.png")<br>

二、主要配置JDK路径，在目录结构中打开./conf/zhiqim.xml：<br><br>
![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_02.png "JDK配置.png")<br>

三、配置/git仓库根目录和HTTP/HTTPS的端口，在目录结构中打开./conf/zhiqim.xml：<br><br>
![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_03.png "httpd配置.png")<br>

四、启动：<br><br>
![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_04.png "启动.png")<br>

<br>

### gitsolo功能展示图解
---------------------------------------
一、首页（在./conf/zhiqim.xml中配置的port，如http://localhost）<br><br>
如图所示，默认记住账号：<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_00.png "操作员管理.png")

二、自带的操作员管理和角色/部门权限功能<br><br>
操作员管理、角色管理（角色成员，角色权限）和部门管理（部门成员，部门权限）<br>
还有，操作日志查询/操作员在线列表<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_05.png "操作员管理.png")

三、项目信息管理功能<br><br>
项目的增删改查和转让等功能。<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_06.png "项目信息管理功能.jpg")

四、项目成员管理功能<br><br>
项目成员的添加和删除功能。<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_07.png "项目成员管理功能.jpg")

五、项目代码仓库功能<br><br>
增加，修改，删除，重命名和迁移仓库功能。<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_08.png "项目代码仓库功能.jpg")

六、设置独立密钥功能<br><br>
开启仓库独立密钥，加强保密性。<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_10.png "设置独立密钥功能.jpg")

七、我的项目动态功能<br><br>
可以查看我创建的项目动态以及向我汇报的项目的动态。<br>

![输入图片说明](https://zhiqim.org/download/zhiqim_products/v1.6.0/readme/gitsolo_readme_09.png "我的项目动态功能.jpg")

<br>

### 知启蒙技术框架与交流
---------------------------------------
![知启蒙技术框架架构图](https://zhiqim.org/project/images/101431_93f5c39d_2103954.jpeg "知启蒙技术框架架构图.jpg")<br><br>
QQ群：加入QQ交流群，请点击[【458171582】](https://jq.qq.com/?_wv=1027&k=5DWlB3b) <br><br>
教程：欲知更多gitsolo，[【请戳这里】](https://zhiqim.org/project/zhiqim_products/gitsolo/tutorial/index.htm)
